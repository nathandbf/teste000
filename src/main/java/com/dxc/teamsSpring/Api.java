package com.dxc.teamsSpring;



import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping

public class Api {

	@GetMapping(path = "/TestGet")
	public String exemploGet() {
		return "Hello World";
	}

}
